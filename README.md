This repo contains the configuration and artifacts necessary to build A9OS Server for X86_64 with Jenkins.

The config.xml file should be placed within the subdirectory corresponding to the project in Jenkins' home directory, i.e., ~/.jenkins/jobs/a9os-server-x86_64/config.xml

Reload any configurations from the Jenkins management panel:

1. http://jenkins:42/manage# 
2. "Reload Configuration from Disk"
3. Modify the configuration to use your credentials
4. Click 'Build now'
